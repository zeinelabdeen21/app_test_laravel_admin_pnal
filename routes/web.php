<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\Dashboard\Admin\LoginController;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


// Route::group(
//     [
//         'prefix' => LaravelLocalization::setLocale(),
//         'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
//     ],
//     function () {

Route::prefix('admin')->middleware('guestAdmin')->group(function () {
    Route::get('login', [LoginController::class, 'get']);
    Route::post('login_post', [LoginController::class, 'post']);

});

Route::prefix('dashboard')->name('dashboard.')->middleware('admin')->group(function () {
    // home page
    Route::get('/', [HomeController::class, 'index'])->name('home');
    // Route::get('lang/{lang}', ['ar' => 'lang.switch', 'uses' => LanguageController::class, 'switchLang']);

    Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);


});

//     }
// );
// Route::get('/', function () {
//     return view('dashboard/dashboard');
// });

// php artisan migrate

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// php artisan make:migration create_admins_table
// php artisan migrate
// php artisan make:controller Dashboard/Admin/AdminsController --model=Admins
// php artisan make:middleware DashboardGuest
