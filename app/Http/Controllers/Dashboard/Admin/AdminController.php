<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Models\Admins;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Validator;

class AdminController extends Controller
{
    public function index()
    {
        $data = Admins::all();
        return view('dashboard.admin.index', compact('data'));
    }
    public function create()
    {

        return view('dashboard.admin.create');
    }
    public function store()
    {
        // dd(request()->all());
        $this->validate(
            request(),
            [
                'username' => 'required|string|min:3',
                'email' => 'required|email|unique:admins,email',
                'password' => 'required|string|confirmed',
            ]
        );
        $create = Admins::create([
            'username' => request()->username,
            'email' => request()->email,
            'password' => Hash::make(request()->password),
        ]);
        if (!$create) {
            return back()->with('error', 'حدث شئ ما خطأ يرجى المحاولة مرة أخرى');
        }
        return redirect('dashboard')->with('success', 'تم إضافة المدير بنجاح');
    }

    public function delete($id)
    {

        Validator::make(
            ['id' => $id],
            ['id' => 'required|integer|exists:admins,id'],
            []
        )->validate();

        $admin = Admins::find(request()->id);
        if (!$admin->delete()) {
            return back()->with('error', 'حدث شئ ما خطأ يرجى المحاولة مرة أخرى');
        }
        return back()->with('success', 'تم حذف المدير');
    }
}